/*
 * Copyright (C) 2012 Stefano Karapetsas
 * Copyright (C) 2012-2021 MATE Developers
 * Authors: Stefano Karapetsas <stefano@karapetsas.com>
 * All Rights Reserved
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "appearance.h"
#include "wm-common.h"
#include "appearance-support.h"

#include <glib.h>
#include <gio/gio.h>

static gboolean
is_program_in_path (const char *program)
{
    char *tmp = g_find_program_in_path (program);
    if (tmp != NULL)
    {
        g_free (tmp);
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

static gboolean
metacity_is_running(void)
{
    gboolean is_running = FALSE;
    gchar *current_wm = NULL;

    current_wm = wm_common_get_current_window_manager ();

    is_running = (g_strcmp0(current_wm, WM_COMMON_METACITY) == 0) ||
                 (g_strcmp0(current_wm, WM_COMMON_COMPIZ_OLD) == 0) ||
                 (g_strcmp0(current_wm, WM_COMMON_COMPIZ) == 0);

    g_free (current_wm);

    return is_running;
}

static void
metacity_theme_apply(const gchar *theme, const gchar *font)
{
    /* set theme, we use gconf and gsettings binaries to avoid schemas and versions issues */
    if (is_program_in_path ("gconftool-2"))
    {
        gchar *gconf_cmd = NULL;

        gconf_cmd = g_strdup_printf("gconftool-2 --set --type string /apps/metacity/general/theme '%s'", theme);
        g_spawn_command_line_async (gconf_cmd, NULL);
        g_free (gconf_cmd);

        gconf_cmd = g_strdup_printf("gconftool-2 --set --type string /apps/metacity/general/titlebar_font '%s'", font);
        g_spawn_command_line_async (gconf_cmd, NULL);
        g_free (gconf_cmd);
    }

    if (is_program_in_path ("gsettings"))
    {
        gchar *gsettings_cmd = NULL;

        /* for GNOME3 */
        gsettings_cmd = g_strdup_printf("gsettings set org.gnome.desktop.wm.preferences theme '%s'", theme);
        g_spawn_command_line_async (gsettings_cmd, NULL);
        g_free (gsettings_cmd);

        gsettings_cmd = g_strdup_printf("gsettings set org.gnome.desktop.wm.preferences titlebar-font '%s'", font);
        g_spawn_command_line_async (gsettings_cmd, NULL);
        g_free (gsettings_cmd);

        /* for metacity >= 3.16 */
        gsettings_cmd = g_strdup_printf("gsettings set org.gnome.metacity theme '%s'", theme);
        g_spawn_command_line_async (gsettings_cmd, NULL);
        g_free (gsettings_cmd);

        /* for metacity >= 3.20 */
        gsettings_cmd = g_strdup_printf("gsettings set org.gnome.metacity.theme name '%s'", theme);
        g_spawn_command_line_async (gsettings_cmd, NULL);
        g_free (gsettings_cmd);
    }
}

/* Change the Plank theme if a Yaru theme is selected */
static void
plank_theme_apply(const gchar *theme, const gchar *layout)
{
    gchar *dconf_cmd = NULL;

    if (g_str_has_prefix (theme, "Yaru"))
    {
        if (g_str_has_prefix (layout, "mutiny"))
        {
            if (g_str_has_suffix (theme, "-dark"))
            {
                dconf_cmd = g_strdup_printf("dconf write /net/launchpad/plank/docks/dock1/theme \"'Mutiny-dark'\"");
            } else
            {
                dconf_cmd = g_strdup_printf("dconf write /net/launchpad/plank/docks/dock1/theme \"'Mutiny-light'\"");
            }
        }
        else
        {
            if (g_str_has_suffix (theme, "-dark"))
            {
                dconf_cmd = g_strdup_printf("dconf write /net/launchpad/plank/docks/dock1/theme \"'Yaru-dark'\"");
            } else
            {
                dconf_cmd = g_strdup_printf("dconf write /net/launchpad/plank/docks/dock1/theme \"'Yaru-light'\"");
            }
        }
        g_spawn_command_line_async (dconf_cmd, NULL);
        g_free (dconf_cmd);
    }
}

/* Change the GtkSourceView Style for Pluma if a Yaru theme is selected */
static void
pluma_theme_apply(const gchar *theme)
{
    gchar *gsettings_cmd = NULL;

    if (g_str_has_prefix (theme, "Yaru"))
    {
        if (g_str_has_suffix (theme, "-dark"))
        {
            gsettings_cmd = g_strdup_printf("gsettings set org.mate.pluma color-scheme 'Yaru-dark'");
        } else
        {
            gsettings_cmd = g_strdup_printf("gsettings set org.mate.pluma color-scheme 'Yaru'");
        }
        g_spawn_command_line_async (gsettings_cmd, NULL);
        g_free (gsettings_cmd);
    }
}

/* If the theme is "-dark", set the GNOME 42 dark preference */
static void
color_scheme_apply(const gchar *theme)
{
    gchar *gsettings_cmd = NULL;

    if (g_str_has_suffix (theme, "-dark"))
    {
        gsettings_cmd = g_strdup_printf("gsettings set org.gnome.desktop.interface color-scheme prefer-dark");
    } else
    {
        gsettings_cmd = g_strdup_printf("gsettings set org.gnome.desktop.interface color-scheme default");
    }
    g_spawn_command_line_async (gsettings_cmd, NULL);
    g_free (gsettings_cmd);
}

static void
marco_theme_changed(GSettings *settings, gchar *key, AppearanceData* data)
{
    gchar *theme = NULL;
    gchar *font = NULL;
    gchar *layout = NULL;
    GSettings * panel_settings;

    theme = g_settings_get_string (settings, MARCO_THEME_KEY);
    font = g_settings_get_string (settings, WINDOW_TITLE_FONT_KEY);

    panel_settings = g_settings_new ("org.mate.panel");
    layout = g_settings_get_string (panel_settings, "default-layout");

    if (is_program_in_path ("gsettings"))
    {
        color_scheme_apply (theme);
        pluma_theme_apply (theme);
    }

    if (is_program_in_path ("dconf"))
    {
        plank_theme_apply (theme, layout);
    }

    if (metacity_is_running ())
    {
        metacity_theme_apply (theme, font);
    }
    g_free (theme);
    g_free (font);
    g_free (layout);
}

void
support_init(AppearanceData* data)
{
    /* needed for wm_common_get_current_window_manager() */
    wm_common_update_window ();
    /* GSettings signals */
    g_signal_connect (data->marco_settings, "changed::" MARCO_THEME_KEY,
                      G_CALLBACK (marco_theme_changed), data);
    g_signal_connect (data->marco_settings, "changed::" WINDOW_TITLE_FONT_KEY,
                      G_CALLBACK (marco_theme_changed), data);
    /* apply theme at start */
    marco_theme_changed (data->marco_settings, NULL, data);
}

void
support_shutdown(AppearanceData* data)
{
    /* nothing to do */
}
